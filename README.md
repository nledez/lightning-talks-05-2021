# Meetup Software Crafts·wo·manship Rennes
# Lightning talks - 20 mai 2021

## Liste des talks proposés :

* **[Titre du talk]** - [Speaker(s)] - [Description rapide]

* **Kanban : brossons le tableau** - Pascal Le Merrer

  La méthode Kanban pour l'IT ce n'est pas seulement un tableau avec des posts-its, loin de là.
  A vrai dire le tableau n'est même pas obligatoire.
  Nous allons revoir rapidement les fondamentaux de cette méthode,
  et pourquoi elle est bien adaptée au développement logiciel tel qu'on le pratique aujourd'hui.

* **Go Generics** - Benoît Masson

  Attendue depuis des années, la généricité s'apprête à être introduite dans le langage Go.
  Nous ferons un petit survol de ce qui est prévu (ou pas), et de ce que cela nous permettra.


* **Moins je vous parle, mieux je me porte** - David Laizé

  Ou pourquoi j'ai un problème (souvent) avec les branches. Et pourquoi le trunk based me plait autant.

* C'est quoi la sécurité ? - Nicolas Ledez

  La sécurité a toujours été vue comme une contrainte. Les backups, ça ne sert
  à rien ou alors on ne teste pas les restaurations (on parle bien de test de
  sauvegarde et pas de tests de restauration). Et puis franchement, la machine qui tombe
  en panne ça n'arrive jamais. Alors, perdre une baie !!! LOL.
  Ça c'était le discours que l'on pouvait entendre en 2020. 10/03/2021 paf le chien.
  Et maintenant, vous allez écouter ce que j'ai à vous dire sur la sécurité ? :)
